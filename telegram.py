"""
The Friendly Foodbot
    A telegram bot that is able to recommend recipes to a user based on ingredients that they fancy.
    Created by Bas Vogelzang and Maarten Vos.
"""

import time
import urllib
import re
import random
from recipe_scrapers import scrap_me
from urllib.request import urlopen
import json
import requests
from nltk.corpus import wordnet
import html

SCRAPERS = ["AllRecipes", "BBCFood", "BBCGoodFood", "BonAppetit", "ClosetCooking", "Cookstr", "Epicurious", "FineDiningLovers",
    "FoodRepublic", "HundredAndOneCookbooks", "JamieOliver", "MyBakingAddiction", "PaniniHappy", "RealSimple", "SimplyRecipes",
    "SteamyKitchen", "TastyKitchen", "ThePioneerWoman", "TheVintageMixer", "TwoPeasAndTheirPod", "WhatsGabyCooking"]

TOKEN = "321851859:AAFm6GFJWFuR-mTmb-Gq9jxOAggwTzJnGKM"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)
GREETING_KEYWORDS = ("hello", "hi", "greetings", "sup", "what's up",)
GREETING_RESPONSES = ["Hi", "Hey", "Good day to you!", "Greetings"]
nr_of_intermediate_responses = 0
favourite = []
def get_url(url):
    """
    Make a request to an URL and return its response.
    """
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content

def pre_processing(text):
    """
    Convert a string to a lowercase string that only contains letters and digits to make sure that the text does not
    contain weird characters.
    """
    return "".join(re.findall("[a-zA-Z0-9 ]+", text)).lower()

def check_sentiment(text):
    """
    Checks the sentiment of a text and returns True when the sentiment is positive and False when it is negative.
    """
    url = 'http://text-processing.com/api/sentiment/'
    if "yes" in text:
        return True
    elif "no" in text:
        return False
    else:
        # If 'yes' or 'no' is not present in the text, use the text-processing api to check
        # whether the text has a positive or negative sentiment.
        r = requests.post(url, data="".join(["text=",text]))
        return not r.json()["label"] == "neg"

def get_json_from_url(url):
    """
    Returns the JSON text from a specified url.
    """
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates(offset=None):
    """
    Returns a JSON text that contains the updates since the specified offset that were made in the telegram-app
    (in which an update is a message that was sent by the user of the chatbot).
    """
    url = URL + "getUpdates"
    if offset:
        url += "?offset={}".format(offset)
    js = get_json_from_url(url)
    return js


def get_last_update_id(updates):
    """
    Returns the ID number of the last made update.
    """
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

def give_recipe_information(recipe,chat):
    """
    When the user has chosen a recipe and confirmed that he would like to make it, provide him with ingredient
    information and instructions on how to create the recipe.
    """

    # First send the user an image of the chosen recipe and asks him for confirmation.
    send_message(recipe["image_url"], chat)
    send_message("".join(["You have chosen to create ",html.unescape(recipe["title"])]), chat)
    send_message("Would you like to make this?", chat)
    response = waiting_for_response()

    # Check if the user's response is positive or negative. This assumes that a user wants to make this recipe when the
    # sentiment of his answer is positive.
    sentiment = check_sentiment(response)
    if(sentiment):
        # If the answer is positive, provide the user with the ingredients and instructions. This is only possible
        # if the url from the recipe can be scraped. If this is not the case, the url of the recipe is returned and
        # the user can click it himself.
        url = recipe["source_url"]
        if any(scraper.lower() in url for scraper in SCRAPERS):
             scraped = scrap_me(url)

             try:
                ingredients = '\n'.join(map(str, html.unescape(scraped.ingredients())))
                ingredients = ingredients.replace('ADVERTISEMENT', '')  # Some websites add ADVERTISEMENT after each
                                                                        # ingredient. This is removed here.
                instructions = html.unescape(scraped.instructions().replace('\n', '\n\n'))

                # Send the instructions to the user.
                send_message("To make this recipe, you need the following ingredients: ", chat)
                send_message(ingredients, chat)

                # Wait a short time to make the user aware that the ingredients are sent before sending the instructions.
                time.sleep(2.5)

                # Send the instructions to the user.
                send_message(instructions, chat)
                
             except AttributeError:
                send_message("".join(["We can not provide you with the right info. For more information see: ", url]), chat)


             # Ask to add the recipe to favourites or delete it from it
             if recipe in favourite:
                 send_message("Do you want to delete this recipe from your favorites?", chat)
                 response = waiting_for_response()
                 if(check_sentiment(response)):
                     favourite.remove(recipe)
                     send_message("The recipe has successfully been removed. Please type in new ingredients or type 'favourite' to see your saved recipes.", chat)
                 else:
                     send_message("Please type in new ingredients.", chat)
             else:
                 send_message("Do you want to add this recipe to your favorites?", chat)
                 response = waiting_for_response()
                 if (check_sentiment(response)):
                     favourite.append(recipe)
                     send_message("The recipe has successfully been added! Please type in new ingredients or type 'favourite' to see your saved recipes.", chat)
                 else:
                     send_message("Please type in new ingredients.", chat)

        else:
            # Send the url to the user if it cannot be scraped.
            send_message(recipe["source_url"], chat)

    else:
        # The user does not like the ingredients, return to the main menu.
        send_message("Sad to hear that, please fill in new ingredients.", chat)

def give_latest_update(updates):
    """
    Returns the latest sent message, or None if no message has been sent yet.
    """
    if len(updates["result"]) > 0:
        return updates["result"][-1]["message"]
    else:
        return None

def waiting_for_response():
    """
    Waits until the user has entered a response and returns this response.
    """
    global nr_of_intermediate_responses
    nr_of_intermediate_responses += 1 # A global variable to keep track of the responses that are made outside of the
                                      # main menu, to keep the code synchronized.
    updates = get_updates(None)
    previousMessage = give_latest_update(updates)

    # Wait while no new message has been posted by the user.
    while (give_latest_update(updates) == previousMessage):
        updates = get_updates(None)
        time.sleep(0.5)

    # Return the new response.
    response = pre_processing(updates["result"][-1]["message"]["text"])
    return response

def get_ingredients(text):
    """
    Returns a list of all the ingredients in a specified text.
    """
    ingredients = [word if is_ingredient(word) else '' for word in text.split()]
    return list(filter(None, ingredients))

def get_greeting(text):
    """
    Returns a list of all the greetings in a specified text.
    """
    greeting = [word if word in GREETING_KEYWORDS else '' for word in text.split()]
    return list(filter(None, greeting))


def search_recipe(ingredients, chat, i):
    """
    Find a suitable recipe for the ingredients that the user entered.
    """
    # Find and print five (starting with the specified i) recipes that use these ingredients.
    data = parse_json("+".join(ingredients))
    end = i + 4
    while(i <= data["count"] and i <= end):
        send_message("".join([str(i), ". ", html.unescape(data["recipes"][i-1]["title"])]),chat)
        i += 1

    # If any recipes were found for the specified ingredients:
    if (data["count"] > 0):

        # Give the user the option to view more recipes if there are more recipes in the data.
        if (i >= data["count"]):
            send_message("No more recipes were found, please type the number of the recipe that you would like to see or type 'abort' to stop.", chat)
        else:
            send_message("Type the number of the recipe that you like to see, type 'more' for more recipes or type 'abort' to stop.",chat)
        response = waiting_for_response()

        # If the user wants to view more recipes and there are more recipes, call this function with the new i.
        if (response == "more"):
            if not (i >= data["count"]):
                search_recipe(ingredients,chat, i)
            else:
                send_message("Sorry, no more recipes were found. Please choose one of the provided recipes.", chat)
                search_recipe(ingredients, chat, i-5)

        # If the user wants to abort, return him to the part of the bot where he can choose new ingredients.
        elif (response == "abort"):
            send_message("Aborting...", chat)
            time.sleep(0.5)
            send_message("Please type some new ingredients", chat)

        # If the user has entered a valid digit, give him the recipe information of the corresponding recipe.
        elif (response.isdigit()):
            if(0 < int(response) < i):
               give_recipe_information(data["recipes"][int(response)-1],chat)
            else:
                send_message("Sorry, your response was invalid. Please choose again", chat)
                search_recipe(ingredients, chat, i-5)

        # If the user entered a non-valid response, ask him to try once more.
        else:
            send_message("Sorry, I was not able to understand your response. Please try again", chat)
            search_recipe(ingredients, chat, i-5)

    # If no recipes were found for these ingredients, let the user enter new ingredients.
    else:
        send_message("I am sorry, no recipes were found for these ingredients", chat)


def respond(update,chat):
    """
    Respond to the message of the user.
    """
    global nr_of_intermediate_responses
    text = pre_processing(update)
    ingredients = get_ingredients(text)
    greeting = get_greeting(text)
    recognizable_text = False
    nr_of_intermediate_responses = 0

    # The message from the user contains a greeting so send him a greeting back.
    if (greeting):
        recognizable_text = True
        send_message(random.choice(GREETING_RESPONSES),chat)

    # The message from the user contains ingredients so find a recipe for these ingredients.
    if (ingredients):
        recognizable_text = True
        ingredient_string = ", ".join(ingredients).replace(''.join([", ", ingredients[-1]]), ''.join([" and ", ingredients[-1]]))
        send_message(''.join(["Searching for a recipe with ",ingredient_string, "..."]),chat)
        time.sleep(2)
        search_recipe(ingredients, chat, 1)

    # If you want to see the favourites
    if ("fav" in text):
        recognizable_text = True
        if not favourite:
            send_message("List is empty, please search for recipes first", chat)
        else:
            favourite_string = ""
            for i, fav in enumerate(favourite):
                favourite_string += ''.join([str(i+1),'. ',html.unescape(fav["title"]),'\n'])
            send_message(favourite_string, chat)
            response = waiting_for_response()

            # If the user has entered a valid digit, give him the recipe information of the corresponding recipe.
            if (response.isdigit()):
                if (1 <= int(response) <= len(favourite)):
                    give_recipe_information(favourite[int(response) - 1], chat)
                else:
                    send_message("Sorry, your response was invalid. Sending you back to the main menu.", chat)
            else:
                send_message("Sorry, your response was invalid. Sending you back to the main menu.", chat)

    # The message contains nothing recognizable.
    if not (recognizable_text):
        send_message("Please type in some ingredients, or type 'favourite' to see your saved recipes!", chat)


def get_last_chat_id_and_text(updates):
    """
    Returns the text and the chat ID of the latest made update.
    """
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)


def send_message(text, chat_id):
    """
    Send a specified text message to a chat with a specified chat_id.
    """
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)


def parse_json(word):
    """
    Returns the JSON text of a specified query from the food2fork-api.
    """
    u = urlopen("http://food2fork.com/api/search?key=c0a1a472c27343352c7e7035c470de21&q="+word)
    content = u.read()
    data = json.loads(content)
    return data


def is_ingredient(word):
    """
    Returns True if a word is an ingredient and False if not.
    """
    reject_synsets = ['vitamin.n.01']
    reject_synsets = set(wordnet.synset(w) for w in reject_synsets)
    accept_synsets = ['food.n.01', 'food.n.02','meal.n.01', 'meal.n.02', 'dish.n.02']
    accept_synsets = set(wordnet.synset(w) for w in accept_synsets)

    # If a word is not a noun, it cannot be an ingredient.
    for word_synset in wordnet.synsets(word, wordnet.NOUN):
        all_synsets = set(word_synset.closure(lambda s: s.hypernyms()))
        all_synsets.add(word_synset)

        # A meal or dish or vitamin is not an ingredient.
        for synset in reject_synsets:
            if synset in all_synsets:
                return False

        # An ingredient is a food that is not a meal or a dish or a vitamin.
        for synset in accept_synsets:
            if synset in all_synsets:
                return True
    return False


def main():
    """
    Waits for each message sent by a user and responds to it.
    """
    global nr_of_intermediate_responses

    # Make sure that the code starts fresh and does not uses updates from previous sessions.
    updates = get_updates(None)
    if(len(updates["result"]) > 0):
        last_update_id = get_last_update_id(updates)
    else:
        last_update_id = None

    # Check for messages and respond to them.
    while True:
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            chat = updates["result"][-1]["message"]["chat"]["id"]
            respond(updates["result"][-1]["message"]["text"], chat)
            last_update_id += nr_of_intermediate_responses
        time.sleep(0.5)

if __name__ == '__main__':
    main()


