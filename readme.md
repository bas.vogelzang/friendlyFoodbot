friendlyFoodbot

The chatbot is a culinairy master chef. The bot has 2 main purposes, it can keep track of your favourite recipes and you can find new recipes based on ingredients or certain dishes that you want to make. It is able to extract culinairy terms from the input and uses the food2fork api to find matching recipes. After this, the bot gives 5 possible recipes(if available). These recipes all come from different sources and these are scraped to get the right ingredients and instructions. The user can either pick one ingredient, or ask for more recipes. Once a recipes has been chosen, the bot provides the user with a nice image. If the user is satisfied with how the image looks, the bot provides the user with the ingredients and instructions. At this stage it is also possible to add or remove the recipe from your list of favourite recipes.If the user asks for the favourite list, the bot will give all the added recipes. Where the user can pick a recipe.

If we had more time, we would add a better understanding of natural language and reacting on this, also learning . The bot is only able to greet and it is able to understand if the response is affermative or not. For example if the user wants to make the recipe or add it to favourites. But we would like that the bot really understood the user. This will probably reduce the amount of pretyped responses.

See https://imgur.com/a/KIohO for example conversations.



